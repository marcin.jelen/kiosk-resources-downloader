package com.disupport.config;

import com.disupport.model.KioskId;
import com.disupport.service.remote.HttpConfigService;

import java.io.FileInputStream;
import java.util.Properties;
import java.util.logging.Logger;

public class ApplicationConfig {

    private static final String PROPERTY_FILE = "application.properties";

    private static final ApplicationConfig INSTANCE = new ApplicationConfig();

    static Logger logger = Logger.getLogger(ApplicationConfig.class.getName());

    private Properties appProperties;

    private KioskId kioskId;

    public static ApplicationConfig getInstance() {
        return INSTANCE;
    }

    private ApplicationConfig() {
        try {
            this.kioskId = HttpConfigService.getKioskConfig();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            appProperties = new Properties();
            appProperties.load(new FileInputStream(PROPERTY_FILE));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public KioskId getKioskId() {
        return kioskId;
    }

    public String getUser() {
        String value = appProperties.getProperty("user");
        if (value != null) {
            return value.trim();
        } else {
            logger.warning("Unable to retrieve 'user' property");
            return null;
        }
    }

    public String getPassword() {
        String value = appProperties.getProperty("password");
        if (value != null) {
            return value.trim();
        } else {
            logger.warning("Unable to retrieve 'password' property");
            return null;
        }
    }

    public String getServer() {
        String value = appProperties.getProperty("server");
        if (value != null) {
            return value.trim();
        } else {
            logger.warning("Unable to retrieve 'server' property");
            return null;
        }
    }

    public String getSecurePort() {
        String value = appProperties.getProperty("securePort");
        if (value != null) {
            return value.trim();
        } else {
            logger.warning("Unable to retrieve 'securePort' property");
            return null;
        }
    }

    public String getNonSecurePort() {
        String value = appProperties.getProperty("nonSecurePort");
        if (value != null) {
            return value.trim();
        } else {
            logger.warning("Unable to retrieve 'nonSecurePort' property");
            return null;
        }
    }

    public boolean isHttps() {
        String value = appProperties.getProperty("https");

        if (value != null) {
            return Boolean.parseBoolean(value.trim());
        } else {
            logger.warning("Unable to retrieve 'https' property");
            return true;
        }
    }

    public boolean isTrustAllCertificates() {
        String value = appProperties.getProperty("trustAllCertificates");

        if (value != null) {
            return Boolean.parseBoolean(value.trim());
        } else {
            logger.warning("Unable to retrieve 'trustAllCertificates' property");
            return true;
        }
    }

}
