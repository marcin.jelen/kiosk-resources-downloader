package com.disupport.config;

import com.disupport.service.remote.WebDavService;

public class WebDavServiceConfig {

    private String userName = "disupport";
    private String password = "#di-remote082020";
    private String serverName = WebDavService.serverName;
    private String subPathPrefix = "";
    private boolean useHTTPS = true;
    private String securePort = WebDavService.serverPort;
    private String noSecurePort = "7777";

    private boolean trustAllCertificates = true;

    private static WebDavServiceConfig INSTANCE;

    public static WebDavServiceConfig getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new WebDavServiceConfig();
        }
        return INSTANCE;
    }

    public static WebDavServiceConfig getInstance(String userName, String password, String serverName,
                                                     String subPathPrefix, boolean useHTTPS, String port,
                                                     boolean trustAllCertificates) {
        if(INSTANCE == null) {
            INSTANCE = new WebDavServiceConfig(userName, password, serverName, subPathPrefix, useHTTPS,
                    port, trustAllCertificates);
        } else {
            INSTANCE.init(userName, password, serverName, subPathPrefix, useHTTPS,
                    port, trustAllCertificates);
        }
        return INSTANCE;
    }

    public String getNoSecurePort() {
        return noSecurePort;
    }

    public void setNoSecurePort(String noSecurePort) {
        this.noSecurePort = noSecurePort;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getSubPathPrefix() {
        return subPathPrefix;
    }

    public void setSubPathPrefix(String subPathPrefix) {
        this.subPathPrefix = subPathPrefix;
    }

    public boolean isUseHTTPS() {
        return useHTTPS;
    }

    public void setUseHTTPS(boolean useHTTPS) {
        this.useHTTPS = useHTTPS;
    }

    public String getSecurePort() {
        return securePort;
    }

    public void setSecurePort(String securePort) {
        this.securePort = securePort;
    }

    public boolean isTrustAllCertificates() {
        return trustAllCertificates;
    }

    public void setTrustAllCertificates(boolean trustAllCertificates) {
        this.trustAllCertificates = trustAllCertificates;
    }

    private void init(String userName, String password, String serverName,
                               String subPathPrefix, boolean useHTTPS, String port,
                               boolean trustAllCertificates) {
        if(userName != null) {
            this.userName = userName;
        }
        if(password != null) {
            this.password = password;
        }
        if(serverName != null) {
            this.serverName = serverName;
        }
        if(subPathPrefix != null) {
            this.subPathPrefix = subPathPrefix;
        }
        if(securePort != null) {
            this.securePort = securePort;
        }
        this.useHTTPS = useHTTPS;
        this.trustAllCertificates = trustAllCertificates;
    }

    private WebDavServiceConfig() {

    }

    private WebDavServiceConfig(String userName, String password, String serverName,
                               String subPathPrefix, boolean useHTTPS, String port,
                               boolean trustAllCertificates) {
        init(userName, password, serverName, subPathPrefix, useHTTPS,
                port, trustAllCertificates);
    }
}
