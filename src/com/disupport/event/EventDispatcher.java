package com.disupport.event;

import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class EventDispatcher <T> {

    private final Lock lock = new ReentrantLock();
    private final ArrayList<EventListener<T>> listeners = new ArrayList<>();

    public void addEventListener(EventListener<T> listener) {
        lock.lock();
        try {
            listeners.add(listener);
        } finally {
            lock.unlock();
        }
    }


    public void removeEventListener(EventListener<T> listener) {
        lock.lock();
        try {
            listeners.remove(listener);
        } finally {
            lock.unlock();
        }
    }

    public void clearListeners() {
        lock.lock();
        try {
            listeners.clear();
        } finally {
            lock.unlock();
        }
    }

    public void dispatchEvent(T eventArgs) {
        lock.lock();
        try {
            for (EventListener<T> listener : listeners) {
                listener.eventOccured(eventArgs);
            }
        } finally {
            lock.unlock();
        }
    }

}
