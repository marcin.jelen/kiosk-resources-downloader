package com.disupport.event;

@FunctionalInterface
public interface EventListener <T> {

    public void eventOccured(T eventArgs);

}
