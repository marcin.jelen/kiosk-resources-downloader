package com.disupport.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.Scanner;

public final class FileUtils {

    public static final boolean writeFile(final String path, final String data) {
        FileOutputStream fs = null;
        try {
            fs = new FileOutputStream(path);
            final OutputStreamWriter out = new OutputStreamWriter(fs, "UTF-8");
            out.write(data);
            out.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static final String readFile(final String path) {
        final File catalogFile = new File(path);
        String content = null;
        if(catalogFile.exists()) {
            try {
                content = new Scanner(catalogFile).useDelimiter("\\Z").next();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return content;
    }

}
