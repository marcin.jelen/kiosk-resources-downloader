package com.disupport.utils;

import com.disupport.data.PositionInputStream;
import com.disupport.service.remote.WebDavService;
import com.github.sardine.Sardine;
import com.github.sardine.SardineFactory;
import com.github.sardine.impl.SardineImpl;
import com.github.sardine.impl.handler.VoidResponseHandler;
import com.github.sardine.impl.io.ContentLengthInputStream;
import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.util.EntityUtils;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Map;

public class SardineUtils {

    public static Sardine getSardine(final String username, final String password) {
        return SardineFactory.begin(username, password);
    }

    public static Sardine getSardine(final String username, final String password, final Proxy proxy) {
       return new SardineImpl(username, password, proxy != null ? proxy.getProxySelector(WebDavService.serverName) : null) {
            @Override
            protected Registry<ConnectionSocketFactory> createDefaultSchemeRegistry() {
                return RegistryBuilder.<ConnectionSocketFactory>create()
                        .register("http", this.createDefaultSocketFactory())
                        .register("https", this.createDefaultSecureSocketFactory())
                        .build();
            }
            @Override
            protected ConnectionSocketFactory createDefaultSecureSocketFactory() {
                try {
                    final SSLContext context = SSLContext.getInstance("TLS");
                    final X509TrustManager trustManager = new X509TrustManager() {
                        public void checkClientTrusted(X509Certificate[] xcs, String string) {}
                        public void checkServerTrusted(X509Certificate[] xcs, String string) {}
                        public X509Certificate[] getAcceptedIssuers() {
                            return new X509Certificate[0];
                        }
                    };
                    context.init(null, new TrustManager[]{ trustManager }, null);
                    return new SSLConnectionSocketFactory(context, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
                } catch (NoSuchAlgorithmException | KeyManagementException e) {
                    e.printStackTrace();
                }
                return super.createDefaultSecureSocketFactory();
            }
            @Override
            protected <T> T execute(HttpRequestBase request, ResponseHandler<T> responseHandler) throws IOException {

                if (request.isAborted()) {
                    request.reset();
                }
                return super.execute(request, responseHandler);
            }
            @Override
            public ContentLengthInputStream get(String url, Map<String, String> headers) throws IOException {

                final HttpGet get = new HttpGet(url);
                for (String header : headers.keySet()) {
                    get.addHeader(header, headers.get(header));
                }
                final HttpResponse response = this.execute(get);
                final VoidResponseHandler handler = new VoidResponseHandler();
                try {
                    handler.handleResponse(response);
                    final PositionInputStream positionInputStream = new PositionInputStream(response.getEntity().getContent()) {
                        public void close() throws IOException {
                            if (getPosition() == response.getEntity().getContentLength()) {
                                EntityUtils.consume(response.getEntity());
                            } else {
                                get.abort();
                            }
                        }
                    };
                    return new ContentLengthInputStream(positionInputStream, response.getEntity().getContentLength());
                } catch (IOException ex) {
                    get.abort();
                    throw ex;
                }
            }
        };
    }
}
