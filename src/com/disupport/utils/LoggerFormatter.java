package com.disupport.utils;

import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;


public class LoggerFormatter extends Formatter {
    @Override
    public String format(final LogRecord record) {
        return record.getSourceClassName()+"::"
                +record.getSourceMethodName()+"::"
                +new Date(record.getMillis())+"\n";
    }
}
