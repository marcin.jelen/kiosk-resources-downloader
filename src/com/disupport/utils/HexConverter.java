package com.disupport.utils;

import java.util.HashMap;

public final class HexConverter {

    private static final char[] HexCV = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
    private static final HashMap<String, Integer> ReCV = new HashMap<>();
    static {
        ReCV.put("0", 0); ReCV.put("1", 1); ReCV.put("2", 2); ReCV.put("3", 3);
        ReCV.put("4", 4); ReCV.put("5", 5); ReCV.put("6", 6); ReCV.put("7", 7);
        ReCV.put("8", 8); ReCV.put("9", 9); ReCV.put("A", 10); ReCV.put("B", 11);
        ReCV.put("C", 12); ReCV.put("D", 13); ReCV.put("E", 14); ReCV.put("F", 15);
    }
    public static String ConvertToHexString(final byte[] tab) {
        String lanc = "";
        for (int i=0; i<tab.length; i++) {
            lanc += HexCV [  (tab[i] & 0xf0) >> 4];
            lanc += HexCV [  (tab[i] & 0x0f)     ];
        }
        return lanc;
    }

    public static byte[] ReadFromHexStringByteArray(final String value) {
        final byte[] result = new byte[value.length()/2];
        for (int i=0; i<value.length()/2; i++) {
            Integer rezi = ReCV.get(value.substring(2*i, 2*i+1))<<4;
            rezi |= ReCV.get(value.substring(2*i+1, 2*i+2));
            result[i] = rezi.byteValue();
        }

        return result;
    }

}
