package com.disupport.utils;

import com.disupport.model.RemoteFile;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.*;


public final class CatalogXMLMapper {

    private String netId;
    private String stationId;

    private final static CatalogXMLMapper INSTANCE = new CatalogXMLMapper();

    private CatalogXMLMapper() {
    }

    public static CatalogXMLMapper getInstance(final String netId, final String stationId) {
        INSTANCE.netId = netId;
        INSTANCE.stationId = stationId;
        return INSTANCE;
    }

    private void mapFileList(final List<RemoteFile> list, final String categoryName, final List<Node> filesList) {
        Node nodeAttribute;
        for (Node fileNode: filesList) {
            if (fileNode.getNodeName().equalsIgnoreCase("file")) {
                if (fileNode.hasAttributes()) {
                    NamedNodeMap attributes = fileNode.getAttributes();
                    if (attributes != null) {

                        String name = "default";
                        int version = 0;
                        String aspectID = "0";
                        String netID = this.netId;
                        String stationID = this.stationId;

                        nodeAttribute = attributes.getNamedItem("Name");
                        if(nodeAttribute != null) {
                            name = nodeAttribute.getNodeValue();
                        }
                        nodeAttribute = attributes.getNamedItem("Version");
                        if(nodeAttribute != null) {
                            version = Integer.parseInt(nodeAttribute.getNodeValue());
                        }
                        nodeAttribute = attributes.getNamedItem("AspectID");
                        if(nodeAttribute != null) {
                            aspectID = nodeAttribute.getNodeValue();
                        }
                        nodeAttribute = attributes.getNamedItem("NetID");
                        if(nodeAttribute != null) {
                            netID = nodeAttribute.getNodeValue();
                        }
                        nodeAttribute = attributes.getNamedItem("StationID");
                        if(nodeAttribute != null) {
                            stationID = nodeAttribute.getNodeValue();
                        }

                        list.add(new RemoteFile(categoryName, version, name, aspectID, netID, stationID));
                    }
                }
            }
        }
    }
    private List<RemoteFile> getCatalogList(final Node xmlNode) {
        final List<RemoteFile> list = new ArrayList<>();
        final List<Node> nodeList = NodeListWrapper.nodeAsList(xmlNode.getChildNodes());

        String categoryName;

        for (Node node: nodeList) {
            if (node.getNodeName().equalsIgnoreCase("category")) {
                if (node.hasAttributes()) {
                    final NamedNodeMap attributes = node.getAttributes();
                    final Node attribute = attributes.getNamedItem("Name");
                    if(attribute != null) {
                        categoryName = attribute.getNodeValue();
                        final List<Node> filesList = NodeListWrapper.nodeAsList(node.getChildNodes());
                        mapFileList(list, categoryName, filesList);
                    }
                }
            }
        }
        return list.size() == 0 ? null : list;
    }
    public List<RemoteFile> mapCatalog(final String input) {
        try {
            final Document document = XMLUtils.stringToXmlDocument(input);
            final Element rootElem = document.getDocumentElement();
            if (rootElem.getNodeName().equals("catalog")) {
                return getCatalogList(rootElem);
            }
        } catch (ParserConfigurationException | SAXException | IOException exception) {
            exception.printStackTrace();
        }
        return null;
    }



}
