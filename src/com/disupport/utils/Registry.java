package com.disupport.utils;

import com.disupport.model.ProxySettings;
import com.disupport.model.UpdateTimeRange;
import com.github.sarxos.winreg.HKey;
import com.github.sarxos.winreg.RegistryException;
import com.github.sarxos.winreg.WindowsRegistry;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public final class Registry {

    private static final String registryTree = "SOFTWARE\\WOW6432Node\\di-Support\\Registration";
    private static final String backgroundUpdateTimeRangeFilePath = "C:\\GLOBAL_CONFIG\\updateTimeRange";

    @Deprecated
    public static String getSerialNumber() {
        final ProxySettings proxySettings = new ProxySettings();
        final WindowsRegistry reg = WindowsRegistry.getInstance();
        try {
            return reg.readString(HKey.HKLM, registryTree, "SSN");
        } catch (RegistryException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ProxySettings getProxySettingsFromRegistry() {
        final ProxySettings proxySettings = new ProxySettings();
        final WindowsRegistry reg = WindowsRegistry.getInstance();
        try {
            final String enabled = reg.readString(HKey.HKLM, registryTree, "ProxyEnabled");
            if(enabled != null && enabled.equals("1")) {
                proxySettings.setProxyEnabled(true);
                proxySettings.setProxyServerHost(reg.readString(HKey.HKLM, registryTree, "ProxyServer"));
                proxySettings.setProxyServerPort(Integer
                        .parseInt(reg.readString(HKey.HKLM, registryTree, "ProxyPort")));
                return proxySettings;
            }
        } catch (RegistryException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static int getUpdateZoneId() {
        final WindowsRegistry reg = WindowsRegistry.getInstance();
        String value = null;
        try {
            value = reg.readString(HKey.HKLM, registryTree, "UpdateZone");
        } catch (RegistryException e) {
            e.printStackTrace();
        }
        return value != null ? Integer.parseInt(value) : -1;
    }

    public static UpdateTimeRange getBackgroundUpdateTimeRange() {
        final Path path = Paths.get(backgroundUpdateTimeRangeFilePath);
        try (BufferedReader br = Files.newBufferedReader(path, StandardCharsets.UTF_8)) {
            String line = br.readLine();
            while (line != null) {
                UpdateTimeRange range = null;
                try {
                    range = UpdateTimeRange.GetInstance(line.split(";"));
                    range.setUpdateZone(getUpdateZoneId());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if(range != null && range.getZone() == range.getUpdateZone()) {
                    return range;
                }
                line = br.readLine();
            }

        } catch (final IOException ioe) {
            ioe.printStackTrace();
        }
        return  null;
    }

}
