package com.disupport.utils;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ProxySelector;
import java.net.SocketAddress;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public final class Proxy {

    private String proxyHost = "192.168.0.103";
    private int proxyPort = 3128;

    private static Proxy INSTANCE;

    public static Proxy getInstance(final String proxyHost, final int proxyPort) {
        Proxy i = getInstance();
        i.proxyHost = proxyHost;
        i.proxyPort = proxyPort;
        return i;
    }
    public static Proxy getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new Proxy();
        }
        return INSTANCE;
    }

    private Proxy() {
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public int getProxyPort() {
        return proxyPort;
    }

    public ProxySelector getProxySelector(final String hostFilterName) {
        return new ProxySelector() {
            @Override
            public List<java.net.Proxy> select(URI uri) {
                final ArrayList<java.net.Proxy> list = new ArrayList<>();
                if(uri.toString().contains(hostFilterName)) {
                    /** System.out.println("select for " + uri.toString()); */
                    java.net.Proxy proxy = new java.net.Proxy(java.net.Proxy.Type.HTTP,
                            new InetSocketAddress(proxyHost, proxyPort));
                    list.add(proxy);
                }
                return list;
            }
            @Override
            public void connectFailed(URI uri, SocketAddress sa, IOException ioe) {
                System.out.println("Proxy connection error - URI (host): " + uri.getHost() +
                        "error: " + ioe.getMessage());
            }
        };
    }

}
