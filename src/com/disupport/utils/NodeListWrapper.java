package com.disupport.utils;


import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.RandomAccess;

public final class NodeListWrapper extends AbstractList<Node> implements RandomAccess {

    private final NodeList list;

    public NodeListWrapper(final NodeList list) {
        this.list = list;
    }
    public Node get(int index) {
        return list.item(index);
    }
    public int size() {
        return list.getLength();
    }
    public static List<Node> nodeAsList(final NodeList nodeList) {
        return nodeList.getLength() == 0 ? new ArrayList<>() : new NodeListWrapper(nodeList);
    }
}
