package com.disupport;

import com.disupport.service.download.ResourcesDownloaderManager;
import com.disupport.service.socket.SocketServer;

public class Main {

    public static void main(String[] args) {

        final SocketServer socketServer = new SocketServer();
        socketServer.getDataEventDispatcher().addEventListener((data)->{
            System.out.println(data);
        });
        socketServer.run();

        ResourcesDownloaderManager downloaderManager = ResourcesDownloaderManager.getInstance();
        downloaderManager.startDownloading();

    }
}
