package com.disupport.model;

import java.time.LocalTime;

public class UpdateTimeRange {
    private int zone;
    private int startTime;
    private int endTime;
    private int updateZone;

    public static UpdateTimeRange GetInstance(String[] metadata) {
        final UpdateTimeRange instance = new UpdateTimeRange();
        instance.setZone(Integer.parseInt(metadata[0]));
        instance.setStartTime(instance.getTime(metadata[1]));
        instance.setEndTime(instance.getTime(metadata[2]));
        return instance;
    }

    static UpdateTimeRange GetDefaultInstance() {
        final UpdateTimeRange instance = new UpdateTimeRange();
        instance.setZone(0);
        instance.setStartTime(1200);
        instance.setEndTime(0);
        return instance;
    }

    private UpdateTimeRange() {
    }

    public int getUpdateZone() {
        return updateZone;
    }

    public void setUpdateZone(int updateZone) {
        this.updateZone = updateZone;
    }

    private int getTime(String metadata) {
        final String[] a = metadata.split(":");
        return Integer.parseInt(a[0]) * 60 + Integer.parseInt(a[1]);
    }

    public int getZone() {
        return zone;
    }

    public void setZone(int zone) {
        this.zone = zone;
    }

    public int getStartTime() {
        return startTime;
    }

    public void setStartTime(int startTime) {
        this.startTime = startTime;
    }

    public int getEndTime() {
        return endTime;
    }

    public void setEndTime(int endTime) {
        this.endTime = endTime;
    }

    public boolean isTimeToUpdate() {

        final LocalTime currentTime = LocalTime.now();
        final int minutes = currentTime.getHour() * 60 + currentTime.getMinute();
        final int startTime = getStartTime();
        final int endTime = getEndTime();

        return startTime < endTime ?
                minutes > startTime && minutes < endTime :
                minutes > startTime || minutes < endTime;

    }
}
