package com.disupport.model;

public class RemoteFile {

    private String categoryName;
    private int version;
    private String name;
    private String aspectId;
    private String netId;
    private String stationId;

    public RemoteFile(String categoryName, int version, String name, String aspectId, String netId, String stationId) {

        this.categoryName = categoryName;
        this.version = version;
        this.name = name;
        this.aspectId = aspectId;
        this.netId = netId;
        this.stationId = stationId;

    }

    public String getCategoryName() {
        return categoryName;
    }

    public int getVersion() {
        return version;
    }

    public String getName() {
        return name;
    }

    public String getAspectId() {
        return aspectId;
    }

    public String getNetId() {
        return netId;
    }

    public String getStationId() {
        return stationId;
    }

    public String getLocalFilePath() {
        return netId + "\\" + stationId + "\\" + categoryName + "\\" + name + "\\" + aspectId;
    }

    public String getRemoteFilePath() {
        return netId + "/" + categoryName + "/" + name + "/" + aspectId;
    }

    @Override
    public String toString() {
        return "RemoteFile{" +
                "categoryName='" + categoryName + '\'' +
                ", version=" + version +
                ", name='" + name + '\'' +
                ", aspectId='" + aspectId + '\'' +
                ", netId='" + netId + '\'' +
                ", stationId='" + stationId + '\'' +
                '}';
    }
}
