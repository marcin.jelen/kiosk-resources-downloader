package com.disupport.model;

public class KioskId {

    private String netId;
    private String stationId;

    public String getNetId() {
        return netId;
    }

    public String getStationId() {
        return stationId;
    }

    public KioskId(String netId, String stationId) {
        this.netId = netId;
        this.stationId = stationId;
    }
}
