package com.disupport;

import java.net.*;
import java.io.*;

public class SocketClient {

    public static void main(String[] args) {

        String hostname = "localhost";
        int port = 8080;

        try (Socket socket = new Socket(hostname, port)) {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());;
            objectOutputStream.writeObject("path");
            objectOutputStream.close();

        } catch (UnknownHostException ex) {

            System.out.println("Server not found: " + ex.getMessage());

        } catch (IOException ex) {

            System.out.println("I/O error: " + ex.getMessage());
        }
    }
}
