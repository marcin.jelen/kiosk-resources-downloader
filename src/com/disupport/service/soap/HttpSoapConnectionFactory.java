package com.disupport.service.soap;

import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;

public class HttpSoapConnectionFactory extends SOAPConnectionFactory {

    public HttpSoapConnectionFactory() {
    }


    @Override
    public SOAPConnection createConnection() throws SOAPException {
        return new HttpSoapConnection();
    }
}
