package com.disupport.service.download;

public class MasksDownloader extends ResourcesDownloader{

    @Override
    protected String[] getFileTypes() {
        return new String[]{"s", "m", "b"};
    }

    @Override
    protected String getCatalogPath() {
        return photomasksPath;
    }

    @Override
    protected String getCatalogXMLString() {
        return resourcesService.getMasksCatalogXMLString();
    }


    @Override
    protected String getRemoteCatalogPath() {
        return photomasksRemote + "/";
    }
}
