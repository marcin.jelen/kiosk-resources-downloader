package com.disupport.service.download;

import com.disupport.config.ApplicationConfig;
import com.disupport.model.KioskId;
import com.disupport.model.ProxySettings;
import com.disupport.model.RemoteFile;
import com.disupport.config.WebDavServiceConfig;
import com.disupport.service.remote.RemoteResourcesService;
import com.disupport.service.remote.WebDavService;
import com.disupport.utils.*;
import com.disupport.service.local.ResourcesVersion;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Logger;

public abstract class ResourcesDownloader implements Runnable {

    public static final String generalPath = "C:\\GLOBAL_CONFIG2\\";

    public static final String backgroundsPath = generalPath + "BACKGROUNDS\\";
    public static final String layoutsPath = generalPath + "LAYOUTS\\";
    public static final String clipartsPath = generalPath + "CLIPARTS\\";
    public static final String photoframesPath = generalPath + "PHOTOFRAMES\\";
    public static final String photomasksPath = generalPath + "PHOTOMASKS\\";

    public static final String backgroundsRemote = "BACKGROUNDS";
    public static final String layoutsRemote = "LAYOUTSG5";
    public static final String clipartsRemote = "CLIPARTS";
    public static final String photoframesRemote = "PHOTOFRAMES";
    public static final String photomasksRemote = "PHOTOMASKS";

    protected List<RemoteFile> remoteFileList;

    protected Thread thread = null;
    protected volatile boolean started = false;
    protected volatile boolean finished = false;

    protected RemoteResourcesService resourcesService;
    protected WebDavService webDavService;

    protected abstract String[] getFileTypes();
    protected abstract String getCatalogPath();
    protected abstract String getRemoteCatalogPath();
    protected abstract String getCatalogXMLString();

    static Logger logger;

    private CatalogXMLMapper xmlMapper;


    public ResourcesDownloader() {

        KioskId kioskId = ApplicationConfig.getInstance().getKioskId();
        xmlMapper = CatalogXMLMapper.getInstance(kioskId.getNetId(), kioskId.getStationId());

        Proxy proxy = null;
        ProxySettings proxySettings = Registry.getProxySettingsFromRegistry();
        if(proxySettings != null && proxySettings.isProxyEnabled()) {
            proxy =  Proxy.getInstance(proxySettings.getProxyServerHost(), proxySettings.getProxyServerPort());
        }

        ApplicationConfig applicationConfig = ApplicationConfig.getInstance();

        WebDavServiceConfig config = WebDavServiceConfig.getInstance(applicationConfig.getUser(),
                applicationConfig.getPassword(),
                applicationConfig.getServer(), "", applicationConfig.isHttps(),
                applicationConfig.getSecurePort(), applicationConfig.isTrustAllCertificates());

        resourcesService = RemoteResourcesService.getInstance(kioskId.getNetId(), kioskId.getStationId(), proxy);
        webDavService = WebDavService.getInstance(config, proxy);

    }

    protected Logger getLogger() {
        if(logger == null) {
            logger = Logger.getLogger(ResourcesDownloader.class.getSimpleName());
            //logger.addHandler(getLoggerHandler());
        }
        return logger;
    }

    protected boolean downloadResources() {
        if(!createRemoteFileList()) {
            return false;
        }
        boolean res = true;
        getLogger().info(this.getClass().getName() + " -> begin download resources");
        for (RemoteFile remoteFile: remoteFileList) {
            res &= downloadFile(remoteFile, getFileTypes());
        }
        getLogger().info(this.getClass().getName() + " -> end download resources");
        return res;
    }

    private boolean createRemoteFileList() {
        List<RemoteFile> remoteFiles = getRemoteCatalog();
        if(remoteFiles != null)
            remoteFileList = remoteFiles;
        if(remoteFileList == null)
            remoteFileList = getLocalCatalog();
        if(remoteFileList == null) {
            getLogger().warning("Cant create the remote files catalog");
            return false;
        }
        return true;
    }

    protected int checkCatalogVersion() {
        final int remoteVersion = resourcesService.getLayoutsVersion();
        final int localVersion = ResourcesVersion.getLocalVersion(getCatalogPath());
        if(remoteVersion > localVersion) {
            return remoteVersion;
        }
        return 0;
    }

    protected int checkFileVersion(final RemoteFile file, final String catalogPath) {
        final int remoteVersion = file.getVersion();
        final int localVersion = ResourcesVersion.getLocalVersion(catalogPath);
        if(remoteVersion > localVersion) {
            return remoteVersion;
        }

        return 0;
    }

    protected Handler getLoggerHandler() {
        final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new LoggerFormatter());
        return handler;
    }

    public boolean downloadFile(final RemoteFile file, String[] fileTypes) {
        final String catalogPath = getCatalogPath() + file.getLocalFilePath();
        final int remoteVersion = checkFileVersion(file, catalogPath);
        String localPath = getCatalogPath() + file.getLocalFilePath() + "\\" + fileTypes[0] + ".bg";
        String remotePath = getRemoteCatalogPath() + file.getRemoteFilePath() + "/" + getRemoteFileNamePrefix(fileTypes[0]) + "." + file.getVersion();
        final File localFile = new File(localPath);
        if(remoteVersion > 0 || !localFile.exists()) {
            try {
                webDavService.downloadFile(remotePath, localPath);
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            if(fileTypes.length > 1) {
                localPath = getCatalogPath() + file.getLocalFilePath() + "\\" + fileTypes[1] + ".bg";
                remotePath = getRemoteCatalogPath() + file.getRemoteFilePath() + "/" + getRemoteFileNamePrefix(fileTypes[1]) + "." +  file.getVersion();
                try {
                    webDavService.downloadFile(remotePath, localPath);
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }
            }
            if(fileTypes.length > 2) {
                localPath = getCatalogPath() + file.getLocalFilePath() + "\\" + fileTypes[2] + ".bg";
                remotePath = getRemoteCatalogPath() + file.getRemoteFilePath() + "/" + getRemoteFileNamePrefix(fileTypes[2]) + "." +  file.getVersion();
                try {
                    webDavService.downloadFile(remotePath, localPath);
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }
            }
            if(remoteVersion > 0) {
                ResourcesVersion.setLocalVersion(catalogPath, remoteVersion);
            }
            return true;
        }
        return false;
    }

    protected String getRemoteFileNamePrefix(final String fileType) {
        switch (fileType) {
            case "s": return "SMALL";
            case "m": return "MEDIUM";
            case "b": return "BIG";
        }
        return null;
    }

    protected List<RemoteFile> getLocalCatalog() {
        final String path = getCatalogPath() + "catalog.xml";
        String content = FileUtils.readFile(path);
        return content != null ? xmlMapper.mapCatalog(content) : null;
    }

    protected List<RemoteFile> getRemoteCatalog() {
        final int remoteVersion = checkCatalogVersion();
        final String path = getCatalogPath() + "catalog.xml";
        File directory = new File(getCatalogPath());
        boolean created = true;
        if (!directory.exists()){
            created = directory.mkdir();
        }
        File catalogFile = new File(path);
        if(created && remoteVersion > 0 || !catalogFile.exists()) {
            String xmlCatalog = getCatalogXMLString();
            if (xmlCatalog != null) {
                final boolean written = FileUtils.writeFile(path, xmlCatalog);
                if(written) {
                    if(remoteVersion > 0) {
                        ResourcesVersion.setLocalVersion(getCatalogPath(), remoteVersion);
                    }
                    return xmlMapper.mapCatalog(xmlCatalog);
                }
            }
        }
        return null;
    }

    public boolean start() {
        finished = false;
        try {
            thread = new Thread(this, this.getClass().getName());
            if (thread != null) {
                thread.start();
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void stop() {
        try {
            if (thread != null) {
                thread.interrupt();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        started = false;
    }

    @Override
    public void run() {

        started = true;
        finished = false;
        boolean success = false;

        while (!success) {
            success = downloadResources();

            try {
                Thread.sleep(success ? 3600000 : 60000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            success = false;
        }
        finished = true;
        started = false;
    }

}
