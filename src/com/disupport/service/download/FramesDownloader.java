package com.disupport.service.download;

public class FramesDownloader extends ResourcesDownloader {

    @Override
    protected String[] getFileTypes() {
        return new String[]{"s"};
    }

    @Override
    protected String getCatalogPath() {
        return photoframesPath;
    }

    @Override
    protected String getCatalogXMLString() {
        return resourcesService.getFramesCatalogXMLString();
    }

    @Override
    protected String getRemoteCatalogPath() {
        return photoframesRemote + "/";
    }


}
