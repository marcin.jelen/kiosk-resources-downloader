package com.disupport.service.download;

public class ClipartsDownloader extends ResourcesDownloader {

    @Override
    protected String[] getFileTypes() {
        return new String[]{"s"};
    }

    @Override
    protected String getCatalogPath() {
        return clipartsPath;
    }

    @Override
    protected String getRemoteCatalogPath() {
        return clipartsRemote + "/";
    }

    @Override
    protected String getCatalogXMLString() {
        return resourcesService.getClipartsCatalogXMLString();
    }


}
