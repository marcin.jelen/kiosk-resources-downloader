package com.disupport.service.download;

public class ResourcesDownloaderManager {

    private final LayoutsDownloader layoutsDownloader = new LayoutsDownloader();
    private final ClipartsDownloader clipartsDownloader = new ClipartsDownloader();
    private final MasksDownloader masksDownloader = new MasksDownloader();
    private final FramesDownloader framesDownloader = new FramesDownloader();

    private final static ResourcesDownloaderManager INSTANCE = new ResourcesDownloaderManager();

    public static ResourcesDownloaderManager getInstance() {
        return INSTANCE;
    }

    private ResourcesDownloaderManager() {

    }

    public void startDownloading() {
        layoutsDownloader.start();
        /*clipartsDownloader.start();
        masksDownloader.start();
        framesDownloader.start();*/
    }
}
