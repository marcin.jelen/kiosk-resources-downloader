package com.disupport.service.download;

import com.disupport.model.RemoteFile;

public class BackgroundsDownloader extends ResourcesDownloader {

    @Override
    protected String[] getFileTypes() {
        return new String[]{"s", "m", "b"};
    }

    @Override
    protected String getCatalogPath() {
        return backgroundsPath;
    }

    @Override
    protected String getRemoteCatalogPath() {
        return backgroundsRemote + "/";
    }

    @Override
    protected String getCatalogXMLString() {
        return resourcesService.getClipartsCatalogXMLString();
    }

    @Override
    public boolean downloadFile(final RemoteFile file, String[] fileTypes) {



        return super.downloadFile(file, fileTypes);
    }


}
