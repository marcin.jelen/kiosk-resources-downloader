package com.disupport.service.download;

import com.disupport.model.RemoteFile;
import com.disupport.service.local.ResourcesVersion;
import com.disupport.utils.HexConverter;
import com.disupport.utils.NodeListWrapper;
import com.disupport.utils.XMLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LayoutsDownloader extends ResourcesDownloader {

    private final BackgroundsDownloader backgroundsDownloader = new BackgroundsDownloader();

    @Override
    protected String[] getFileTypes() {
        return new String[]{"s"};
    }

    @Override
    protected String getCatalogPath() {
        return layoutsPath;
    }

    @Override
    protected String getCatalogXMLString() {
        return resourcesService.getLayoutsCatalogXMLString();
    }

    @Override
    protected String getRemoteCatalogPath() {
        return layoutsRemote + "/";
    }

    @Override
    public boolean downloadFile(final RemoteFile file, String[] fileTypes) {
        final String catalogPath = getCatalogPath() + file.getLocalFilePath();
        final int remoteVersion = checkFileVersion(file, catalogPath);
        final String localPath = getCatalogPath() + file.getLocalFilePath() + "\\" + fileTypes[0] + ".bg";
        final String remotePath = getRemoteCatalogPath() + file.getRemoteFilePath() + "/" + getRemoteFileNamePrefix(fileTypes[0]) + "." + file.getVersion();
        final File localFile = new File(localPath);
        boolean backgroundDownloaded = true;
        if(remoteVersion > 0 || !localFile.exists()) {
            try {
                final byte[] data = webDavService.downloadFile(remotePath, localPath);
                List<RemoteFile> backgrounds = getLayoutBackgroundsList(data);


                for (RemoteFile remoteFile: backgrounds) {
                    backgroundDownloaded &= backgroundsDownloader.downloadFile(remoteFile, backgroundsDownloader.getFileTypes());
                }

            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            if(remoteVersion > 0) {
                ResourcesVersion.setLocalVersion(catalogPath, remoteVersion);
            }
            return backgroundDownloaded;// todo
        }
        return false;
    }

    private List<RemoteFile> getLayoutBackgroundsList(byte[] layout) {

        List<RemoteFile> list = new ArrayList<>();
        try {
            final Document document = XMLUtils.getDocumentFromByteArray(layout);
            Element rootElem = document.getDocumentElement();
            if (rootElem.getNodeName().equals("layout")) {
                final List<Node> nodeList = NodeListWrapper.nodeAsList(rootElem.getChildNodes());
                for (Node node: nodeList) {
                    if (node.hasAttributes()) {
                        final NamedNodeMap attributes = node.getAttributes();
                        final Node attribute = attributes.getNamedItem("backid");

                        String bckId = attribute.getNodeValue();
                        byte[] bytes = HexConverter.ReadFromHexStringByteArray(bckId);
                        String str = new String(bytes);
                        Document backgroundDocument = XMLUtils.getDocumentFromByteArray(bytes);
                        final List<Node> bckgNodeList = NodeListWrapper.nodeAsList(backgroundDocument.getDocumentElement().getChildNodes());
                        String categoryName = "default";
                        String name = "default";
                        int version = 0;
                        String aspectID = "0";
                        String netID = null;
                        String stationID = null;

                        for (Node bckgNode: bckgNodeList) {

                            if(bckgNode.getNodeName().equals("groupName")) {
                                categoryName = bckgNode.getTextContent();
                                continue;
                            }
                            if(bckgNode.getNodeName().equals("backgroundName")) {
                                name = bckgNode.getTextContent();
                                continue;
                            }
                            if(bckgNode.getNodeName().equals("aspectID")) {
                                aspectID = bckgNode.getTextContent();
                                continue;

                            }
                            if(bckgNode.getNodeName().equals("version")) {
                                String nodeValue = bckgNode.getTextContent();
                                version = Integer.parseInt(nodeValue);
                                continue;
                                //version = Integer.parseInt(bckgNode.getNodeValue());
                            }
                            if(bckgNode.getNodeName().equals("netID")) {
                                netID = bckgNode.getTextContent();
                                continue;
                            }
                            if(bckgNode.getNodeName().equals("stationID")) {
                                stationID = bckgNode.getTextContent();
                                continue;
                            }

                        }
                        String s = new String(bytes);

                        list.add(new RemoteFile(categoryName, version, name, aspectID, netID, stationID));
                        //System.out.println(s);

                    }
                }
                nodeList.size();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return list.size() == 0 ? null : list;
    }

    private List<String> getBackgroundsFromLayout(final byte[] layoutBuffer) {
        return null;
    }


}
