package com.disupport.service.remote;

import com.disupport.service.soap.HttpSoapConnection;
import com.disupport.service.soap.HttpSoapConnectionFactory;
import com.disupport.utils.Proxy;
import org.apache.commons.lang3.StringEscapeUtils;

import javax.xml.soap.*;
import java.io.ByteArrayOutputStream;

public abstract class RemoteService {

    final protected String namespaceURI = "http://tempuri.org";

    protected Proxy proxy;

    protected abstract SOAPMessage createSoapEnvelope(final String soapAction)
            throws SOAPException;

    protected abstract String getFormattedXMLString(final String input, final String soapAction);

    protected String callSoapWebService(final String endpointUrl, final String soapAction) {
        try {

            final HttpSoapConnectionFactory soapConnectionFactory = new HttpSoapConnectionFactory();
            final SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            final HttpSoapConnection httpSoapConnection = (HttpSoapConnection) soapConnection;

            if(proxy != null) {
                httpSoapConnection.setProxy(proxy.getProxyHost(), proxy.getProxyPort());
            }

            final SOAPMessage soapResponse = httpSoapConnection.call(createSOAPRequest(soapAction), endpointUrl);
            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            soapResponse.writeTo(outputStream);
            final String stringValue = outputStream.toString("UTF-8");
            httpSoapConnection.close();
            final String unescapedString = StringEscapeUtils.unescapeXml(stringValue);

            return getFormattedXMLString(unescapedString, soapAction);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    protected SOAPMessage getSoapMessage() throws SOAPException {
        final MessageFactory messageFactory = MessageFactory.newInstance();
        return messageFactory.createMessage();
    }

    protected SOAPMessage createSOAPRequest(final String soapAction)
            throws SOAPException {

        final SOAPMessage soapMessage = createSoapEnvelope(soapAction);
        final MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", namespaceURI + "/" + soapAction);
        soapMessage.saveChanges();

        return soapMessage;
    }

}
