package com.disupport.service.remote;

import com.disupport.model.KioskId;
import com.disupport.utils.NodeListWrapper;
import com.disupport.utils.XMLUtils;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.*;
import java.io.IOException;
import java.util.List;

@Deprecated
public class RemoteKioskConfigService extends RemoteService {

    final private static String configServiceEndpointUrl =
            "http://layouts.di-support.com:10800/PhotoService/FotoToGoConfigInterface.asmx";
    final private static String getConfigurationAction = "GetKioskConfiguration";

    private final static RemoteKioskConfigService INSTANCE = new RemoteKioskConfigService();

    public static RemoteKioskConfigService getInstance() {
        return INSTANCE;
    }

    public KioskId getKioskConfiguration() {
        return mapConfig(callSoapWebService(configServiceEndpointUrl, getConfigurationAction));
    }

    @Override
    protected String getFormattedXMLString(final String input, final String soapAction) {
        return StringUtils.substringBetween(input,
                "<GetKioskConfigurationResponse xmlns=\"http://tempuri.org/\">", "</GetKioskConfigurationResponse>");

    }

    @Override
    protected SOAPMessage createSoapEnvelope(final String soapAction)
            throws SOAPException {

        final SOAPMessage soapMessage = getSoapMessage();
        final SOAPPart soapPart = soapMessage.getSOAPPart();
        final String namespace = "confNamespace";

        final SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration(namespace, namespaceURI+"/");

        final SOAPBody soapBody = envelope.getBody();
        soapBody.addChildElement(soapAction, namespace);
        return soapMessage;
    }

    private KioskId mapConfig(final String input) {
        KioskId kioskId = null;
        try {
            final Document document = XMLUtils.stringToXmlDocument(input);
            final Element rootElem = document.getDocumentElement();

            if (rootElem.getNodeName().equals("GetKioskConfigurationResult")) {
                final List<Node> nodeList = NodeListWrapper.nodeAsList(rootElem.getChildNodes());
                String netId = "0";
                String stationId = "0";
                for (Node node: nodeList) {
                    if (node.getNodeName().equalsIgnoreCase("NetID")) {
                        netId = node.getTextContent();
                    } else if(node.getNodeName().equalsIgnoreCase("StationID")) {
                        stationId = node.getTextContent();
                    }
                }
                kioskId = new KioskId(netId, stationId);
            }
        } catch (ParserConfigurationException | SAXException | IOException exception) {
            exception.printStackTrace();
        }
        return kioskId;
    }

    private RemoteKioskConfigService() {
    }
}
