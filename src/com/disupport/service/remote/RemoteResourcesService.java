package com.disupport.service.remote;
import com.disupport.utils.Proxy;
import org.apache.commons.lang3.StringUtils;

import javax.xml.soap.*;

public class RemoteResourcesService extends RemoteService {

    final private static String serviceEndpointUrl = "http://layouts.di-support.com:10800/BackgroundManager/";

    final private static String layoutEndpointUrl = serviceEndpointUrl + "LayoutsG5Service.asmx";
    final private static String maskEndpointUrl = serviceEndpointUrl + "PhotoMasksService.asmx";
    final private static String frameEndpointUrl = serviceEndpointUrl + "PhotoFramesService.asmx";
    final private static String clipartEndpointUrl = serviceEndpointUrl + "ClipartsG5Service.asmx";

    final private static String getCatalogAction = "GetCatalog";
    final private static String getCatalogVersionAction = "GetCatalogVersion";

    private final static RemoteResourcesService INSTANCE = new RemoteResourcesService();

    private String netId;
    private String stationId;

    public static RemoteResourcesService getInstance(final String netId, final String stationId,
                                                     final Proxy proxy) {
        INSTANCE.netId = netId;
        INSTANCE.stationId = stationId;
        INSTANCE.proxy = proxy;
        return INSTANCE;
    }

    public static RemoteResourcesService getInstance() {
        return INSTANCE;
    }

    public String getLayoutsCatalogXMLString() {
        return callSoapWebService(layoutEndpointUrl, getCatalogAction);
    }

    public String getFramesCatalogXMLString() {
        return callSoapWebService(frameEndpointUrl, getCatalogAction);
    }

    public String getMasksCatalogXMLString() {
        return callSoapWebService(maskEndpointUrl, getCatalogAction);
    }

    public String getClipartsCatalogXMLString() {
        return callSoapWebService(clipartEndpointUrl, getCatalogAction);
    }

    public int getLayoutsVersion() {
        return Integer.parseInt(callSoapWebService(layoutEndpointUrl, getCatalogVersionAction));
    }

    public int getFramesVersion() {
        return Integer.parseInt(callSoapWebService(frameEndpointUrl, getCatalogVersionAction));
    }

    public int getMasksVersion() {
        return Integer.parseInt(callSoapWebService(maskEndpointUrl, getCatalogVersionAction));
    }

    public int getClipartsVersion() {
        return Integer.parseInt(callSoapWebService(clipartEndpointUrl, getCatalogVersionAction));
    }

    @Override
    protected String getFormattedXMLString(final String input, final String soapAction) {
        switch (soapAction) {
            case getCatalogAction:
                return StringUtils.substringBetween(input, "<GetCatalogResult>", "</GetCatalogResult>");
            case getCatalogVersionAction:
                return StringUtils.substringBetween(input, "<GetCatalogVersionResult>", "</GetCatalogVersionResult>");
        }
        return null;
    }

    @Override
    protected SOAPMessage createSoapEnvelope(final String soapAction)
            throws SOAPException {

        final SOAPMessage soapMessage = getSoapMessage();
        final SOAPPart soapPart = soapMessage.getSOAPPart();
        final String namespace = "resNamespace";

        final SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration(namespace, namespaceURI+"/");

        final SOAPBody soapBody = envelope.getBody();
        final SOAPElement soapBodyElem = soapBody.addChildElement(soapAction, namespace);
        final SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("netID", namespace);
        final SOAPElement soapBodyElem2 = soapBodyElem.addChildElement("stationID", namespace);
        soapBodyElem1.addTextNode(netId);
        soapBodyElem2.addTextNode(stationId);
        return soapMessage;
    }

}
