package com.disupport.service.remote;

import com.disupport.config.WebDavServiceConfig;
import com.disupport.data.WebDavInputStream;
import com.disupport.utils.Proxy;
import com.disupport.utils.SardineUtils;
import com.github.sardine.DavResource;
import com.github.sardine.Sardine;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.http.client.utils.URIBuilder;

import java.io.*;
import java.util.HashMap;
import java.util.List;

public class WebDavService {

    public static final String serverName = "layouts.di-support.com";
    public static final String serverPort = "443";

    private WebDavServiceConfig serviceConfig;

    private final static WebDavService INSTANCE = new WebDavService();
    public static final int  FILE_BUFFER_SIZE= 4096;
    private static final String WEB_DAV_BASE_PATH = "/";

    private WebDavService() {}

    private ThreadLocal<Sardine> sardine;

    protected Sardine getSardine() {
        return sardine.get();
    }

    public static WebDavService getInstance(WebDavServiceConfig serviceConfig, Proxy proxy) {
        INSTANCE.serviceConfig = serviceConfig;
        INSTANCE.sardine = ThreadLocal.withInitial(() -> INSTANCE.getWebDavClient(proxy));
        return INSTANCE;
    }

    private Sardine getWebDavClient(Proxy proxy) {
        return serviceConfig.isUseHTTPS() ?
                SardineUtils.getSardine(serviceConfig.getUserName(), serviceConfig.getPassword(), proxy) :
                SardineUtils.getSardine(serviceConfig.getUserName(), serviceConfig.getPassword());
    }

    public void listDirectory(String remotePath) {

        String path = buildWebdavPath(remotePath);
        Sardine client = getSardine();
        List<DavResource> resources = null;

        try{
            resources = client.list(path);
        } catch (Exception e) {
            System.out.println(e);
        }

        if(resources != null) {
            for (DavResource res : resources) {
                System.out.println(res);
            }
        }
    }

    public boolean pathExists(String remotePath) {

        String path = buildWebdavPath(remotePath);
        Sardine client = getSardine();

        try {
            boolean exists = client.exists(path);
            client.shutdown();
            return exists;
        } catch (IOException e) {

            try {
                client.shutdown();
            } catch (IOException ex) {
                System.out.println("error in closing sardine connector:" + ex);
            }
            throw new RuntimeException(e);//todo
        }
    }

    public InputStream downloadFile(String remotePath) throws IOException {

        String path = buildWebdavPath(remotePath);
        Sardine client = getSardine();
        WebDavInputStream inputStream;

        try {
            inputStream = new WebDavInputStream(client, client.get(path, getGETHeaders()));
        } catch (IOException e) {
            throw new RuntimeException(e);//todo
        } finally {
            try {
                client.shutdown();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return inputStream;
    }


    public byte[] downloadFile(String remotePath, String downloadFilePath) throws IOException {
        byte[] buffer = null;
        byte[] data = null;
        String path = buildWebdavPath(remotePath);
        Sardine client = getSardine();
        File directoryPath = new File(downloadFilePath.substring(0, downloadFilePath.lastIndexOf("\\")));
        InputStream inputStream = null;

        boolean dirCreated = true;
        if (!directoryPath.exists()) {
            dirCreated = directoryPath.mkdirs();
        }

        if(!dirCreated)
            throw new RuntimeException("todo"); // todo

        try {
            inputStream = client.get(path, getGETHeaders());
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            if(inputStream != null) {
                buffer = new byte[FILE_BUFFER_SIZE];
                int bytesRead;
                File targetFile = new File(downloadFilePath);
                try (OutputStream outputStream = new FileOutputStream(targetFile)) {
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        outputStream.write(buffer, 0, bytesRead);
                        byteArrayOutputStream.write(buffer, 0, bytesRead);

                    }
                    data = byteArrayOutputStream.toByteArray();
                    byteArrayOutputStream.flush();
                    outputStream.flush();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }

        return data;
    }

    private HashMap<String, String> getGETHeaders() {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Translate", "f");
        return headers;
    }
    private String buildWebdavPath(String remotePath) {
        URIBuilder builder= new URIBuilder()
                .setScheme(serviceConfig.isUseHTTPS() ? "https" : "http")
                .setHost(serviceConfig.getServerName())
                .setPort(Integer.parseInt(serviceConfig.isUseHTTPS() ?
                        serviceConfig.getSecurePort() : serviceConfig.getNoSecurePort()))
                .setPath(WEB_DAV_BASE_PATH + remotePath);

        return builder.toString();
    }
}
