package com.disupport.service.remote;

import com.disupport.model.KioskId;
import com.disupport.utils.NodeListWrapper;
import com.disupport.utils.XMLUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.util.List;

public class HttpConfigService {

    public static KioskId getKioskConfig() throws Exception {

        String input = getConfigString();
        final Document document = XMLUtils.stringToXmlDocument(input);
        final Element rootElem = document.getDocumentElement();
        KioskId kioskId = null;
        if (rootElem.getNodeName().equals("ConfigData")) {
            final List<Node> nodeList = NodeListWrapper.nodeAsList(rootElem.getChildNodes());
            String netId = "0";
            String stationId = "0";
            for (Node node: nodeList) {
                if (node.getNodeName().equalsIgnoreCase("NetID")) {
                    netId = node.getTextContent();
                } else if(node.getNodeName().equalsIgnoreCase("StationID")) {
                    stationId = node.getTextContent();
                }
            }
            kioskId = new KioskId(netId, stationId);
        }

        return kioskId;

    }
    private static String getConfigString() throws Exception {

        final HttpPost post =
                new HttpPost("http://127.0.0.1:10800/PhotoService/FotoToGoConfigInterface.asmx/GetKioskConfiguration");
        String response = null;

        try (final CloseableHttpClient httpClient = HttpClients.createDefault();
             final CloseableHttpResponse httpResponse = httpClient.execute(post)) {

            response = EntityUtils.toString(httpResponse.getEntity());
        }
        return response;

    }



}
