package com.disupport.service.socket;

import com.disupport.event.EventDispatcher;

import java.io.*;
import java.net.*;


public class SocketServer extends Thread {

    private final EventDispatcher<String> dataEventDispatcher = new EventDispatcher<>();

    private int port;

    public SocketServer() {
        this.port = 8080;
    }

    public EventDispatcher<String> getDataEventDispatcher() {
        return dataEventDispatcher;
    }

    public void run() {
        try (ServerSocket serverSocket = new ServerSocket(port)) {

            System.out.println("Server is listening on port " + port);

            while (true) {
                Socket socket = serverSocket.accept();

                InputStream inputStream = socket.getInputStream();
                ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
                String path = "";
                try {
                     path = (String) objectInputStream.readObject();
                    dataEventDispatcher.dispatchEvent(path);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
