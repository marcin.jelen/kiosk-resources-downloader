package com.disupport.service.local;

import java.io.File;
import java.io.FilenameFilter;

public final class ResourcesVersion {

    public static final boolean resetVersions(String folder) {

        final File dir = new File(folder);

        try {
            if (dir.exists()) {
                final File[] files = dir.listFiles((dir1, name) -> name.endsWith(".version"));
                for (int i = 0; i < files.length; i++)
                    files[i].delete();
            }
            dir.mkdirs();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return dir.exists();
    }

    public static final boolean deleteVersions(String folder) {

        try {
            final File dir = new File(folder);
            final FilenameFilter filter = (dir1, name) -> name.endsWith(".version");

            final String[] localFiles = dir.list(filter);
            for (int i = 0; i < localFiles.length; i++) {
                final String filename = folder + "/" + localFiles[i];
                final File f = new File(filename);
                f.delete();
            }

            return true;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static final int getLocalVersion(String folder) {
        int version = 0;
        try {
            final File dir = new File(folder);

            final FilenameFilter filter = (dir1, name) -> name.endsWith(".version");

            final String[] localFiles = dir.list(filter);
            if (localFiles.length == 0) {
                version = 0;
            } else {
                final String[] parts = localFiles[0].split("\\.");
                version = Integer.parseInt(parts[0]);
            }
        } catch (Exception ex) {
            version = 0;
        }

        return version;
    }

    public static final boolean setLocalVersion(String folder, int version) {
        try {
            if (deleteVersions(folder)) {
                final File f = new File(folder + "/" + version + ".version");
                f.createNewFile();
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }
}
