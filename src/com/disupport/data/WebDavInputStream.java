package com.disupport.data;

import com.github.sardine.Sardine;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.io.input.ProxyInputStream;

public class WebDavInputStream extends ProxyInputStream {

    private final Sardine sardine;

    public WebDavInputStream(final Sardine sardine, final InputStream in) {
        super(in);
        this.sardine= sardine;
    }

    @Override
    public void close() throws IOException {
        super.close();
        sardine.shutdown();
    }

}
